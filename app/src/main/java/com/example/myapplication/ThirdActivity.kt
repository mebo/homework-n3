package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class ThirdActivity : AppCompatActivity() {

    private lateinit var hobbyEditText: EditText
    private lateinit var finishButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_third)

        hobbyEditText = findViewById(R.id.editTextHobby)
        finishButton = findViewById(R.id.buttonFinish)


        var age = ""
        var name = ""

        if (intent.extras != null) {

            age = intent.extras?.getString("AGE", "").toString()
            name = intent.extras?.getString("NAME", "").toString()
        }

        finishButton.setOnClickListener{

            val hobby = hobbyEditText.text.toString()

            val intent = Intent(this, FinishActivity::class.java)
            intent.putExtra("AGE", age)
            intent.putExtra("NAME", name)
            intent.putExtra("HOBBY", hobby)
            startActivity(intent)
            finish()
        }
    }
}